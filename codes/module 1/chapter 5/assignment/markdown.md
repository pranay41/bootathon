# Microcontroller Interfaced with Display Devices

<br>

## __TASK 1 :__
#### Blinking LED using 8051 Microcontroller.
<hr>
A light-emitting diode (LED) is a semiconductor device or simply a PN junction diode that emits visible light when an electric current passes through it in the forward direction. The amount of light output is directly proportional to the forward current till a certain value of current.

![circuit](1.png "circuit")

>#### How Light Emitting Diode (LED) works?
Light Emitting Diode (LED) works only in forward bias condition. When Light Emitting Diode (LED) is forward biased, the free electrons from n-side and the holes from p-side are pushed towards the junction.
When free electrons reach the junction or depletion region, some of the free electrons recombine with the holes in the positive ions. We know that positive ions have less number of electrons than protons. Therefore, they are ready to accept electrons. Thus, free electrons recombine with holes in the depletion region. In a similar way, holes from p-side recombine with electrons in the depletion region.

![electrons](2.png "LED circuit")

Because of the recombination of free electrons and holes in the depletion region, the width of the depletion region decreases. As a result, more charge carriers will cross the p-n junction. Some of the charge carriers from p-side and n-side will cross the p-n junction before they recombine in the depletion region. For example, some free electrons from n-type semiconductor cross the p-n junction and recombine with holes in p-type semiconductor. In a similar way, holes from p-type semiconductor cross the p-n junction and recombine with free electrons in the n-type semiconductor. Thus, recombination takes place in the depletion region as well as in p-type and n-type semiconductor. The free electrons in the conduction band release energy in the form of light before they recombine with holes in the valence band.

>### How LED will be connected to 8051 practically?
The below diagram shows the interfacing of a LED with a port pin of microcontroller Interfacing_of_led

![microcontroller](3.png "Micro-controller Interface")

<hr>
<br>

## __TASK 2 :__
 #### Display a digit on seven segment display using 8051 microcontrollers.
<hr>
The 7-segment display consists of seven LEDs arranged in a rectangular fashion. Each of the seven LEDs is called a segment because when illuminated the segment forms part of a numerical digit (both Decimal and Hex) to be displayed. An additional 8th LED is sometimes used within the same package which is the indication of a decimal point(DP), when two or more 7-segment displays are connected together numbers greater than ten can be displayed.
So by forward biasing the appropriate pins of the LED segments in a particular order, some segments will be glowing and others will remain as it is, allowing the desired character pattern of the number to be generated on the display. This then allows us to display each of the ten decimal digits 0 to 9 on the same 7-segment display.

Now accordingly terminals are taken common, so there are two types of display:
+ Common Cathode display
+ Common Anode display

>### 1. The Common Cathode (CC) –
In the common cathode display, all the cathode connections of the LED segments are joined together to logic “0” or ground. The individual segments are illuminated by application of a “HIGH”, or logic “1” signal via a current limiting resistor to forward bias the individual Anode terminals (a-g).

![cathode-display](4.png "Common Cathode 7-segment Display")

_Common Cathode Decoding Table_
|Decimal|a|b|c|d|e|f|g|
|:----:|----|----|----|----|----|----|----|
|0|1|1|1|1|1|1||
|1||1|1|||||
|2|1|1||1|1||1|
|3|1|1|1|1|||1|
|4||1|1|||1|1|
|5|1||1|1||1|1|
|6|1||1|1|1|1|1|
|7|1|1|1|||||
|8|1|1|1|1|1|1|1|
|9|1|1|1|1||1|1|

>### 2. The Common Anode (CA) –
In the common anode display, all the anode connections of the LED segments are joined together to logic “1”. The individual segments are illuminated by applying a ground, logic “0” or “LOW” signal via a current limiting resistor to the Cathode of the particular segment (a-g).

![anode-display](5.png "Common Anode 7-segment Display")


_Common Anode Decoding Table_
|Decimal|a|b|c|d|e|f|g|
|:----:|----|----|----|----|----|----|----|
|0|0|0|0|0|0|0||
|1||0|0|||||
|2|0|0||0|0||0|
|3|0|0|0|0|||0|
|4||0|0|||0|0|
|5|0||0|0||0|0|
|6|0||0|0|0|0|0|
|7|0|0|0|||||
|8|0|0|0|0|0|0|0|
|9|0|0|0|0||0|0|

In general, common anode displays are more popular as many logic circuits can sink more current than they can source. Also note that a common cathode display is not a direct replacement in a circuit for a common anode display and vice versa, as it is the same as connecting the LEDs in reverse, and hence light emission will not take place.
Depending upon the decimal digit to be displayed, the particular set of LEDs is forward biased. For instance, to display the numerical digit 0, we will need to light up six of the LED segments corresponding to a, b, c, d, e and f. Then the various digits from 0 through 9 can be displayed using a 7-segment display as shown below.

![7-segment-display](6.png "7-segment-display")
<hr>

**Refer the table given below to know the hex values for displaying a specific number on the 7 segment display during the simulation.**

![hex-values](7.png "Hex-Values")

<hr>

### Documentation about 8051 Microcontroller
+ [8051 Overview.pdf](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/src/pdfs-docs/8051%20overview.pdf)
+ [8051 Hardware Overview.pdf](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/src/pdfs-docs/8051%20overview.pdf)
+ [8051 Instruction Set.pdf](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/src/pdfs-docs/8051IS.pdf)

<br>

_[This Lab is Contributed by VLABS](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/license/index%20for%208051%20Microcontroller%20and%20Applications%20Lab.html)_ &copy;

