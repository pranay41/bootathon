//Retrieving values from all textboxes

var t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1")
var t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2")
var t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3")

var t4:HTMLInputElement=<HTMLInputElement>document.getElementById("t4")
var t5:HTMLInputElement=<HTMLInputElement>document.getElementById("t5")
var t6:HTMLInputElement=<HTMLInputElement>document.getElementById("t6")

var t7:HTMLInputElement=<HTMLInputElement>document.getElementById("t7")
var t8:HTMLInputElement=<HTMLInputElement>document.getElementById("t8")
var t9:HTMLInputElement=<HTMLInputElement>document.getElementById("t9")

var t10:HTMLInputElement=<HTMLInputElement>document.getElementById("t10")
var t11:HTMLInputElement=<HTMLInputElement>document.getElementById("t11")
var t12:HTMLInputElement=<HTMLInputElement>document.getElementById("t12")

// Function to add two numbers
function add(){

    var s:number=parseFloat(t1.value)+parseFloat(t2.value);
    if (isNaN(s))
        alert ("Enter Numeric Values");
    else
        t3.value=s.toString();
}

// Function to subtract two numbers
function sub(){
    var s:number=parseFloat(t4.value) - parseFloat(t5.value);
    if (isNaN(s))
        alert ("Enter Numeric Values");
    else
        t6.value=s.toString()  //Converting to type string for output
}

// Function to muliply two numbers
function mul(){
    var s:number=parseFloat(t7.value) * parseFloat(t8.value);
    if (isNaN(s))
        alert ("Enter Numeric Values");
    else
        t9.value=s.toString()
}

// Function to divide two numbers
function div(){
    if (t11.value=="0") 
        alert("Denominator Should not be 0 !!");    // Error Handling For Denominator 0
    else
    {
        var s:number=parseFloat(t10.value) / parseFloat(t11.value); 
        if (isNaN(s))
            alert ("Enter Numeric Values");
        else
            t12.value=s.toString();
    }
}