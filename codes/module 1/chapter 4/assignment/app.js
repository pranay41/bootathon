var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
var t4 = document.getElementById("t4");
var t5 = document.getElementById("t5");
var t6 = document.getElementById("t6");
var t7 = document.getElementById("t7");
var t8 = document.getElementById("t8");
var t9 = document.getElementById("t9");
var t10 = document.getElementById("t10");
var t11 = document.getElementById("t11");
var t12 = document.getElementById("t12");
function add() {
    var s = parseFloat(t1.value) + parseFloat(t2.value);
    if (isNaN(s))
        alert("Enter Numeric Values");
    else
        t3.value = s.toString();
}
function sub() {
    var s = parseFloat(t4.value) - parseFloat(t5.value);
    if (isNaN(s))
        alert("Enter Numeric Values");
    else
        t6.value = s.toString();
}
function mul() {
    var s = parseFloat(t7.value) * parseFloat(t8.value);
    if (isNaN(s))
        alert("Enter Numeric Values");
    else
        t9.value = s.toString();
}
function div() {
    if (t11.value == "0")
        alert("Denominator Should not be 0 !!");
    else {
        var s = parseFloat(t10.value) / parseFloat(t11.value);
        if (isNaN(s))
            alert("Enter Numeric Values");
        else
            t12.value = s.toString();
    }
}
//# sourceMappingURL=app.js.map